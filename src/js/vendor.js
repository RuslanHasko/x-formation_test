// JQuery Library

//= ../../bower_components/jquery/dist/jquery.min.js

// Bootstrap Library

//= ../../bower_components/bootstrap/dist//js/bootstrap.min.js

// AngularJS Framework

//= ../../bower_components/angular/angular.min.js

// AngularJS Routing

//= ../../bower_components/angular-route/angular-route.min.js

// AngularJS Pagination

//= ../../bower_components/angularUtils-pagination/dirPagination.js
