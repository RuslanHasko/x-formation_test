'use strict';

var app = angular.module('statisticApp', ['ngRoute', 'angularUtils.directives.dirPagination']).config(config);
// Routing setup
config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];
function config($routeProvider, $locationProvider, $httpProvider) {
  var partialsUrl = '/views/';
  $routeProvider
    .when('/contributors_statistic', {
      templateUrl: partialsUrl + 'contributorsStatistic.html',
      controller: 'contributorsStatisticController',
      title: "Contributors Statistic",
      pageStage: 'contributorsStatistic'
    })
    .when('/public_repositories_statistic', {
      templateUrl: partialsUrl + 'publicRepositoriesStatistic.html',
      controller: 'publicRepositoriesStatisticController',
      title: "X-Formation Public Repositories Statistic",
      pageStage: 'publicRepositoriesStatistic'
    })
    .otherwise({
        redirectTo: '/contributors_statistic'
    });
    $locationProvider.html5Mode(true);
}
// End routing
// Run setup
app.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
        $rootScope.stagePage = current.$$route.pageStage;
    });
}]);
// End of run setup

// Contributors Statistic Controller
angular.module('statisticApp').controller('contributorsStatisticController', contributorsStatisticController);
contributorsStatisticController.$inject = ['$location', '$scope', '$http'];

function contributorsStatisticController($location, $scope, $http) {
  $scope.sortType = 'contributions';
  $scope.sortReverse = true;
  $scope.searchItem = '';
  $http.get('contributors.json').success(function(data) {
    $scope.contributors = data;
  });
}
// End of Contributors Statistic Controller

// Public Repositories Statistic Controller
angular.module('statisticApp').controller('publicRepositoriesStatisticController', publicRepositoriesStatisticController);
publicRepositoriesStatisticController.$inject = ['$location', '$scope', '$http'];

function publicRepositoriesStatisticController($location, $scope, $http) {
  $scope.sortRepoType = 'forks';
  $scope.sortReverse = false;
  $scope.searchcRepoItem = '';
  $http.get("https://api.github.com/users/" + "x-formation/repos")
    .success(function (data) {
      console.log("Success! :)");
      $scope.repos = data;
  })
    .error(function (data) {
      console.log("Something went wrong! :(");
      console.log(data);
  });
}
// End of Public Repositories Statistic Controller