To install project, please clone it to destination foolder.

ATTENTION!
Node.JS, Bower and Gulp must to be installed in your OS.

Open command line, go to project root directory and run:
npm install (if you want only serving files).
bower install (for developing).

Project running:
In project root directory run: gulp server

For developing, run: gulp --silent